#!/bin/bash

BASE_COOKBOOK=$1
BASE_COOKBOOK_REPO_URL=$2

# Create Chef user
useradd -m chef-solo
usermod -aG wheel chef-solo
touch /etc/sudoers.d/00_elevate_wheel
echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/00_elevate_wheel

# Build and configure ChefDK
mkdir /build
chown -R :wheel /build
chmod 775 /build
cd /build
sudo -u chef-solo git clone https://aur.archlinux.org/chef-dk.git
cd chef-dk
sudo -u chef-solo makepkg -risc --noconfirm
sleep 180
mkdir -p /etc/chef
echo "node_name '$(cat /etc/hostname)'" > /etc/chef/client.rb

# Pull system base cookbook
mkdir -p /home/chef-solo/cookbooks
chown -R chef-solo:chef-solo /home/chef-solo/cookbooks
cd /home/chef-solo/cookbooks
sudo -u chef-solo git clone $BASE_COOKBOOK_REPO_URL

# Converge system
 chef-client --chef-license=accept --local-mode -c /etc/chef/client.rb -o $BASE_COOKBOOK
