#!/bin/bash

ADMIN_USER=$1
# ADMIN_PASS=$2
DOTFILE_REPO_URL=$2
WALLPAPER_REPO_URL=$3



# Create Admin user
useradd -m $ADMIN_USER
usermod -aG wheel $ADMIN_USER
# passwd $ADMIN_USER

# Build dotfiles
cd /home/$ADMIN_USER
sudo -u $ADMIN_USER mkdir /home/$ADMIN_USER/tmpdotfiles
sudo -u $ADMIN_USER git clone --separate-git-dir=/home/$ADMIN_USER/.dotfiles $DOTFILE_REPO_URL /home/$ADMIN_USER/tmpdotfiles
sudo -u $ADMIN_USER rsync --recursive --verbose --exclude '.git' /home/$ADMIN_USER/tmpdotfiles/ /home/$ADMIN_USER/
rm -r /home/$ADMIN_USER/tmpdotfiles
rm -r /home/$ADMIN_USER/.dotfiles

# Build wallpaper folder
sudo -u $ADMIN_USER mkdir -p /home/$ADMIN_USER/images/wallpaper
sudo -u $ADMIN_USER git clone $WALLPAPER_REPO_URL /home/$ADMIN_USER/images/wallpaper
