#!/bin/bash

# Script to backup $HOME

# Variables
DEVICE_NAME=""
LOCAL_USER=""
LAN_STORAGE_IP=""
LAN_STORAGE_UNAME=""

# rsync home directory
BACKUP_FILE="$(date '+%Y%m%d-%H%M')-home-backup.tar.gz"
tar czvf $BACKUP_FILE /home/$LOCAL_USER

# Create initial mountpoint and mount storage
mkdir -p /media/$LOCAL_USER/capsule
mount -t cifs //$LAN_STORAGE_IP/Data /media/$LOCAL_USER/capsule -o username=$LAN_STORAGE_UNAME,sec=ntlm,vers=1.0

# Create the directory on remote storage, if needed
BACKUP_DIR="/media/$LOCAL_USER/capsule/backups/$DEVICE_NAME/$LOCAL_USER/$(date '+%Y/%m/%d')"
mkdir -p $BACKUP_DIR

rsync -vr $BACKUP_FILE $BACKUP_DIR
cp $BACKUP_FILE /media/$LOCAL_USER/capsule/backups/$DEVICE_NAME/$LOCAL_USER/latest.tar.gz

# TODO: Clean up old backups

# Unmount remote storage
umount /media/$LOCAL_USER/capsule

