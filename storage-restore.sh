  #!/bin/bash

# Script to backup $HOME

# Variables
DEVICE_NAME=""
LOCAL_USER=""
LAN_STORAGE_IP=""
LAN_STORAGE_UNAME=""
LATEST=true # Must always be true for now

ORIG_DIR=pwd

# TODO: Add functionality to backup for specific
# timestamps

# Create initial mountpoint and mount storage
mkdir -p /media/$LOCAL_USER/capsule
mount -t cifs //$LAN_STORAGE_IP/Data /media/$LOCAL_USER/capsule -o username=$LAN_STORAGE_UNAME,sec=ntlm,vers=1.0

# Set the backup directory location
if [ "$LATEST" = true ]; then
    BACKUP_FILE="/media/$LOCAL_USER/capsule/backups/$DEVICE_NAME/$LOCAL_USER/latest.tar.gz"
else
    BACKUP_FILE="/media/$LOCAL_USER/capsule/backups/$DEVICE_NAME/$LOCAL_USER/FILENAME-GOES-HERE"
fi

# rsync backup archive
rsync -vr $BACKUP_FILE /tmp
cd /tmp

tar -xvf latest.tar.gz

# Restore home directory
rsync -vr home/$LOCAL_USER/ /home/$LOCAL_USER
rm -rf home/$LOCAL_USER
rm latest.tar.gz

# rsync home directory
# BACKUP_FILE=/home/$LOCAL_USER/"$(date '+%Y%m%d-%H%M')-home-backup.tar.gz"
# tar czvf $BACKUP_FILE /home/$LOCAL_USER

# TODO: Clean up old backups

# Unmount remote storage
umount /media/$LOCAL_USER/capsule

